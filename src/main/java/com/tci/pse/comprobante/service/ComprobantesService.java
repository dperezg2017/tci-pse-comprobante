package com.tci.pse.comprobante.service;

import com.tci.pse.comprobante.config.StorageProperties;
import com.tci.pse.comprobante.controller.web.dto.ComprobanteColaRequest;
import com.tci.pse.comprobante.controller.web.dto.ComprobanteStorageRequest;
import com.tci.pse.comprobante.controller.web.dto.JsonCredentialStorage;

public interface ComprobantesService {

    void enviarNuevaColaDeclaracion (ComprobanteColaRequest comprobanteDto);

    JsonCredentialStorage obtenerCuentaServicioStorage();
    StorageProperties.CredentialProperties obtenerCredencialStorage();
    String enviarStorage(ComprobanteStorageRequest comprobanteStorageRequest, JsonCredentialStorage jsonCredentialStorage, StorageProperties.CredentialProperties propertiesStorage);
}
