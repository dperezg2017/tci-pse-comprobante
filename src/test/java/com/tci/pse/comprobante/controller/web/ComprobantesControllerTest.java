package com.tci.pse.comprobante.controller.web;

import com.tci.pse.comprobante.config.StorageProperties;
import com.tci.pse.comprobante.controller.web.dto.ComprobanteStorageRequest;
import com.tci.pse.comprobante.controller.web.dto.JsonCredentialStorage;
import com.tci.pse.comprobante.service.ComprobantesService;
import com.tci.pse.comprobante.service.ComprobantesServiceImpl;
import com.tci.pse.comprobante.service.PubSubGatewayInterface;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.nio.charset.StandardCharsets;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import org.junit.Rule;
//import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import static org.junit.Assert.assertNotNull;
import com.google.gson.Gson;
@ExtendWith(SpringExtension.class)
@WebMvcTest(ComprobantesController.class)
@ContextConfiguration(classes = {ComprobantesControllerTest.Configuration.class})
public class ComprobantesControllerTest {

	public static class Configuration {

		@Bean
		public ComprobantesService comprobantesService(PubSubGatewayInterface pubSubTemplate, StorageProperties storageProperties){
			return new ComprobantesServiceImpl(pubSubTemplate,storageProperties);
		}

	}

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@MockBean
	private StorageProperties storageProperties;

	@MockBean
	private PubSubGatewayInterface pubSubTemplate;

	@Mock
	private ComprobantesService comprobantesService;

	@InjectMocks
	private ComprobantesServiceImpl comprobantesServiceImpl;

	@InjectMocks
	private final ComprobantesController comprobantesController = new ComprobantesController(comprobantesService);

	@Test
	public void save_inStorage_ok(){
		// Preparing data

		ComprobanteStorageRequest comprobanteStorageRequest = new ComprobanteStorageRequest();
		String msj = "text to test";
		comprobanteStorageRequest.setIdTransaccion(123456789L);
		comprobanteStorageRequest.setNombreDocumento("20112811094-03-B100-1451.zip");
		comprobanteStorageRequest.setXmlZip(msj.getBytes(StandardCharsets.UTF_8));

		StorageProperties.CredentialProperties credentialProperties = new StorageProperties.CredentialProperties();
		credentialProperties.setProjectId("pe-tci-cld-01");
		credentialProperties.setBucket("tci-pse-dev-cpe");
		credentialProperties.setConfigServer("Test-Mockito");

		JsonCredentialStorage jsonCredentialStorage = new JsonCredentialStorage();
		jsonCredentialStorage.setClientId("113016558454450283500");
		jsonCredentialStorage.setClientEmail("tci-pse-sa-dev-storage@pe-tci-cld-01.iam.gserviceaccount.com");
		jsonCredentialStorage.setPrivateKey("-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCjDNby8mOGUxHH\nC5oSS1XW0OkROtRUhUD5HyblqP4QIfflijXY7ry+fwBqf7eicmiEK4zjB09Dr6Q/\nQcmfQNKksW9TQ4S1rTMN7J55PKq365X9KrcJVJmjfWG5JL3hHVW/qMk3FQB8a8zU\nL/HyF634zh6n9orxg+/sPPT746Cj8Gl+9XFWNbmTvkflwlXVpAerovilv5KuFB3q\nq+9C2EVbJocDbdJ3X25OBd62MbeHyGn4p1j4kXm2YVq+vx0s1l5kom2gIncmgKAs\ncXzk+nUqsI9MnMoZpmQm4/0iATrZY7F1pfkAwlJ8V79YmW3zacCogsBcWtWq1qLu\n3CAOzoC9AgMBAAECggEAMHfdH6wJBMGFy9b5jk2hrzE7zxjfTF6uMT/e5q8lEvOe\nBrheiGVzaudC3uWnV11OD1gI0eX23IaGOx2Zy0LQOw/ZqXUtKnpZbl6zvxNXJkQc\nOn0rmtTwtOZNGeW/of7/4fDRG5QdKF89LKgx8kayuv6+oyxJIkEXnXCDXKtOMd0I\nFEnMX+XkwFGW2z9xTPoUi2KM40dS1cuAi191s8fzMwfB8AjVQfwSTuy7kcgt1hzs\n7FpvFx4jR2mUazGBqfxYhe9gbCyQPWKnGdPwOX5z+bWZ6aDbIy6FvlBem6CCUxI2\niw3Ms91P/fKgzwwOwsI6VrBV0Y7yqhLA6rsPu5JuCwKBgQDcnE2KH5k0L88CR7sl\nX3yWX5nHLCrvPjESrtSr1SvBka3Le2EUlQQ2fSOwtr+dSeIQTeH8VZczn64fjKK3\n2eX4P7eoFrKEvjxJOtpbpg3BGUB44RPrgW4SqxMBPKVKHctMCaV74M6umQK4Jfcx\naeAG5tSmgMJ2Ok3oxJ+nNGjRRwKBgQC9NLvvGgpBwKaYXBPjna7KjIXDZCU5R1E0\nSW9R1y9WRkhDeyKwe61nCyfAx2keiqGeFI/SKNhg954i162ERTanzfC2jJxnJlJo\nKjF6wR68pZdiFmzxLM5YqR4JCxCUSYA8TRw5sPjTeTnosiwatzBXa2BeBRVYIGPk\n31P+jsU/2wKBgDXrdgA2bOmWbdAu3AzsY7UVNK67mtrSpofcd1pwRbuP8Zx0UWwI\nZ/kxaNG+OOMmtbwqbPuRp90UQCix5OtutIiSej75HTTYkj+LxOb1lRMHKvzgdk/v\nWInzijeZLQUKdy/kg6/daGbAOyvrC5nmOL03Ge6ANgdThMtJk4P4iU3tAoGAX1Z2\nKEoHxxALIhSrlPL8g2WBdW4973nHKkCijiuO17s8aHxgg2seeiC93D7e8hvKyjv0\nKGKafWnoTgg9j7vMiYuU8ncxzCwcNA6mtgMfuND70doY9QGtzDETSF1B8maC7l+w\njEVnbC/WhTFaVe/etP4PJfELy0cu7sG1sjxAmicCgYEAiD3jdEQhq+qsvQjSGutn\nj+kNBxh90P9vGRYwjduJzyVsjdMawdCCk75vjyHWtgz9cCDKPdHk7G1D3e01Lqt8\ncbovNTa0TiE74Nf43xbq1AuKtQUDApoSfoSJ714+o9CaDeRU39k4iNgSKWpunHAI\ndOCMqPKysZBYI1grtFGz/5s=\n-----END PRIVATE KEY-----\n");
		jsonCredentialStorage.setPrivateKeyId("186c31c6a70d69be73a990fc14b82b338ea16351");

		String expected = "https://www.googleapis.com/storage/v1/b/tci-pse-dev-cpe/o/xml%2F123456789-20112811094-03-B100-1451.zip";

		// Mocks & Stubs configuration
		when(comprobantesService.obtenerCuentaServicioStorage()).thenReturn(jsonCredentialStorage);
		when(comprobantesService.obtenerCredencialStorage()).thenReturn(credentialProperties);

		// Business logic execution

		String link = comprobantesServiceImpl.enviarStorage(comprobanteStorageRequest,jsonCredentialStorage,credentialProperties);
		// Validating mocks behaviour
		// Validating results
		assertEquals(expected,link);
	}

	@Test
	public void save_inStorage_blanks(){
		// Preparing data
		ComprobanteStorageRequest comprobanteStorageRequest = new ComprobanteStorageRequest();
		comprobanteStorageRequest.setIdTransaccion(null);
		comprobanteStorageRequest.setNombreDocumento(null);
		comprobanteStorageRequest.setXmlZip("".getBytes());

		// Mocks & Stubs configuration
			Gson gson = new Gson();
			String input = gson.toJson(comprobanteStorageRequest);

			try {

				URL targetUrl = new URL("http://localhost:8085/comprobante/transmitirComprobante");

				HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
				httpConnection.setDoOutput(true);
				httpConnection.setRequestMethod("POST");
				httpConnection.setRequestProperty("Content-Type", "application/json");

				OutputStream outputStream = httpConnection.getOutputStream();
				outputStream.write(input.getBytes());
				outputStream.flush();

				assertEquals(400, httpConnection.getResponseCode());

			}catch (Exception e){
				assertEquals("Connection refused (Connection refused)",e.getMessage());
			}
		// Business logic execution
		// Validating mocks behaviour
		// Validating results

	}

	@Test
	public void save_inStorage_ok_not_null(){
		// Preparing data

		ComprobanteStorageRequest comprobanteStorageRequest = new ComprobanteStorageRequest();
		String msj = "text to test";
		comprobanteStorageRequest.setIdTransaccion(123456789L);
		comprobanteStorageRequest.setNombreDocumento("20112811094-03-B100-1451.zip");
		comprobanteStorageRequest.setXmlZip(msj.getBytes(StandardCharsets.UTF_8));

		StorageProperties.CredentialProperties credentialProperties = new StorageProperties.CredentialProperties();
		credentialProperties.setProjectId("pe-tci-cld-01");
		credentialProperties.setBucket("tci-pse-dev-cpe");
		credentialProperties.setConfigServer("Test-Mockito");

		JsonCredentialStorage jsonCredentialStorage = new JsonCredentialStorage();
		jsonCredentialStorage.setClientId("113016558454450283500");
		jsonCredentialStorage.setClientEmail("tci-pse-sa-dev-storage@pe-tci-cld-01.iam.gserviceaccount.com");
		jsonCredentialStorage.setPrivateKey("-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCjDNby8mOGUxHH\nC5oSS1XW0OkROtRUhUD5HyblqP4QIfflijXY7ry+fwBqf7eicmiEK4zjB09Dr6Q/\nQcmfQNKksW9TQ4S1rTMN7J55PKq365X9KrcJVJmjfWG5JL3hHVW/qMk3FQB8a8zU\nL/HyF634zh6n9orxg+/sPPT746Cj8Gl+9XFWNbmTvkflwlXVpAerovilv5KuFB3q\nq+9C2EVbJocDbdJ3X25OBd62MbeHyGn4p1j4kXm2YVq+vx0s1l5kom2gIncmgKAs\ncXzk+nUqsI9MnMoZpmQm4/0iATrZY7F1pfkAwlJ8V79YmW3zacCogsBcWtWq1qLu\n3CAOzoC9AgMBAAECggEAMHfdH6wJBMGFy9b5jk2hrzE7zxjfTF6uMT/e5q8lEvOe\nBrheiGVzaudC3uWnV11OD1gI0eX23IaGOx2Zy0LQOw/ZqXUtKnpZbl6zvxNXJkQc\nOn0rmtTwtOZNGeW/of7/4fDRG5QdKF89LKgx8kayuv6+oyxJIkEXnXCDXKtOMd0I\nFEnMX+XkwFGW2z9xTPoUi2KM40dS1cuAi191s8fzMwfB8AjVQfwSTuy7kcgt1hzs\n7FpvFx4jR2mUazGBqfxYhe9gbCyQPWKnGdPwOX5z+bWZ6aDbIy6FvlBem6CCUxI2\niw3Ms91P/fKgzwwOwsI6VrBV0Y7yqhLA6rsPu5JuCwKBgQDcnE2KH5k0L88CR7sl\nX3yWX5nHLCrvPjESrtSr1SvBka3Le2EUlQQ2fSOwtr+dSeIQTeH8VZczn64fjKK3\n2eX4P7eoFrKEvjxJOtpbpg3BGUB44RPrgW4SqxMBPKVKHctMCaV74M6umQK4Jfcx\naeAG5tSmgMJ2Ok3oxJ+nNGjRRwKBgQC9NLvvGgpBwKaYXBPjna7KjIXDZCU5R1E0\nSW9R1y9WRkhDeyKwe61nCyfAx2keiqGeFI/SKNhg954i162ERTanzfC2jJxnJlJo\nKjF6wR68pZdiFmzxLM5YqR4JCxCUSYA8TRw5sPjTeTnosiwatzBXa2BeBRVYIGPk\n31P+jsU/2wKBgDXrdgA2bOmWbdAu3AzsY7UVNK67mtrSpofcd1pwRbuP8Zx0UWwI\nZ/kxaNG+OOMmtbwqbPuRp90UQCix5OtutIiSej75HTTYkj+LxOb1lRMHKvzgdk/v\nWInzijeZLQUKdy/kg6/daGbAOyvrC5nmOL03Ge6ANgdThMtJk4P4iU3tAoGAX1Z2\nKEoHxxALIhSrlPL8g2WBdW4973nHKkCijiuO17s8aHxgg2seeiC93D7e8hvKyjv0\nKGKafWnoTgg9j7vMiYuU8ncxzCwcNA6mtgMfuND70doY9QGtzDETSF1B8maC7l+w\njEVnbC/WhTFaVe/etP4PJfELy0cu7sG1sjxAmicCgYEAiD3jdEQhq+qsvQjSGutn\nj+kNBxh90P9vGRYwjduJzyVsjdMawdCCk75vjyHWtgz9cCDKPdHk7G1D3e01Lqt8\ncbovNTa0TiE74Nf43xbq1AuKtQUDApoSfoSJ714+o9CaDeRU39k4iNgSKWpunHAI\ndOCMqPKysZBYI1grtFGz/5s=\n-----END PRIVATE KEY-----\n");
		jsonCredentialStorage.setPrivateKeyId("186c31c6a70d69be73a990fc14b82b338ea16351");

		String expected = "https://www.googleapis.com/storage/v1/b/tci-pse-dev-cpe/o/xml%2F123456789-20112811094-03-B100-1451.zip";

		// Mocks & Stubs configuration
		// Business logic execution

		String link = comprobantesServiceImpl.enviarStorage(comprobanteStorageRequest,jsonCredentialStorage,credentialProperties);

		// Validating mocks behaviour
		// Validating results
		assertNotNull(link);
	}


}

